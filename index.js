const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const Kafka = require('no-kafka');
const dotEnv = require('dotenv').config({ path: '.env' });
var port = process.env.PORT || 2000;
var router = express.Router();

var pg = require('pg')
var elasticsearch = require('elasticsearch');
let cache = null

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/', router);
app.listen(port);
console.log('Connected to ' + port);

// ES connect
var client = new elasticsearch.Client({
   host: `${process.env.ES_HOST}:9200`
});

// postgres connect
var pool = new pg.Pool({
   user: 'hasapu',
   password: 'hasapu',
   host: `${process.env.POSTGRESQL_HOST}`,
   port: '5432',
   database: 'newsdb'
})

// kafka connect
const producer = new Kafka.Producer({
   connectionString: `kafka://${process.env.KAFKA_CLUSTER_HOST}:9092`
});


pool.query('CREATE TABLE IF NOT EXISTS news(id SERIAL PRIMARY KEY, author VARCHAR(40), body VARCHAR(355), created timestamptz NOT NULL DEFAULT now())', (err, res) => { })


router.use(function (req, res, next) {
   next();
});

// GET
router.route('/news').get(async (req, resp) => {
   const response = await client.search({
      index: 'news',
      type: '_doc',
      body: {
         sort: [{ "created": { "order": "desc" } }],
         size: req.query.limit,
         from: (req.query.page - 1) * req.query.limit,
         query: { match_all: {} }
      }
   });
   if (cache !== null && cache.page === parseInt(req.query.page) && cache.limit === parseInt(req.query.limit) && cache.total === response.hits.total) {
      resp.json(cache);
   } else {
      var results = response['hits']['hits'].map(function (i) {
         return i['_source']['id'];
      });

      if (results.length > 0) {
         const placeholders = results.map(function (i, idx) {
            return '$' + (idx + 1);
         }).join(',');

         // const text = "SELECT * FROM news WHERE id IN (" + placeholders + ") ORDER BY ID DESC"
         const text = "SELECT * FROM news WHERE id IN (" + placeholders + ") ORDER BY ID DESC"
         const values = results

         pool.query(text, values, (err, res) => {
            if (err) {
               console.log(err.stack)
            } else {
               var results = {
                  total: response.hits.total,
                  page: parseInt(req.query.page),
                  limit: parseInt(req.query.limit),
                  data: res.rows
               }
               cache = results;
               resp.json(results);
            }
         })
      } else {
         cache = null;
         resp.json({});
      }
   }





})

// POST
router.route('/news').post((req, res) => {
   var message = sendData(req.body)
   res.json({ message: message });
})


// send data to kafka consumer
function sendData(data) {
   return producer.init().then(() => {
      return producer.send({
         topic: 'news',
         partition: 0,
         message: {
            value: "{" +
               "\"author\":" + "\"" + data.author + "\"" + "," +
               "\"body\":" + "\"" + data.body + "\"" +
               "}"
         }
      });
   });
}