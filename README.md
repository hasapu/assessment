# assessment

this is assessment project by hari sakti putra

environmet:
1. nodejs express
2. kafka
3. elasticsearch
4. postgresql

cara running:
1. install semua package dengan 
   $ npm install
2. setelah selesai jalankan bash
   $ sh running.sh
3. setelah app running, jalankan bash 
   $ restart-consumer.sh
   
   lakukan restart apabila data tidak masuk ke database. Penyebab data tidak masuk ke database adalah network app consumer tidak terhubung dengan server kafka, dikarenakan permasalah concurrency pada internal network docker.

4. apabila terjadi error network docker, jalankan
   $ sh docker-network.sh


Postgresql:
username: hasapu
password: hasapu
host: 127.0.0.1 (for local)
port: 5432
db: newsdb

documentasi api:

https://www.getpostman.com/collections/13b31fa0582e1c429820

host development -> 127.0.0.1
<!-- host staging -> 3.89.251.55  -->

ket:
1. {host}:9200/news (PUT)           -> untuk membuat index pada ES
2. {host}:9200/news/_doc (POST)     -> untuk membuat doc pada ES
3. {host}:9200/news/_doc/_search    -> untuk featching semua data dari ES
4. {host}:2000/news                 -> post author dan body ke database
5. {host}:2000/news?page=1&limit=10 -> filtering data dari ES dan db postgres1l