const Kafka = require('no-kafka');
const dotEnv = require('dotenv').config({ path: '.env' });
var pg = require('pg')
var elasticsearch = require('elasticsearch');
var moment = require('moment')


// connect to postgresql
var pool = new pg.Pool({
   user: 'hasapu',
   password: 'hasapu',
   host: `${process.env.POSTGRESQL_HOST}`,
   port: '5432',
   database: 'newsdb'
})

// connect to ES
var client = new elasticsearch.Client({
   host: `${process.env.ES_HOST}:9200`
});


// connect to Kafka
var consumer = new Kafka.SimpleConsumer({
   connectionString: `kafka://${process.env.KAFKA_CLUSTER_HOST}:9092`
})


// handle data from kafka
var dataHandler = function (messageSet) {
   messageSet.forEach(function (m) {
      var message = m.message.value.toString('utf8')
      var data = JSON.parse(message)

      const text = 'INSERT INTO news(author, body) VALUES($1, $2) RETURNING *'
      const values = [data.author, data.body]


      pool.query(text, values, (err, res) => {
         if (err) {
            console.log(err.stack)
         } else {
            postToES(res.rows[0])
         }
      })
   });
};

// checking if there is message from producer
return consumer.init().then(function () {
   return consumer.subscribe('news', [0], dataHandler);
});

// send data to ES
async function postToES(data) {
   var date = moment(data.created).format('YYYY-MM-DD HH:mm:ss.SSSZ')
   await client.index({
      index: 'news',
      type: '_doc',
      id: data.id,
      body: {
         id: data.id,
         created: date
      }
   })
}
